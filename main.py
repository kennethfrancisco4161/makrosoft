from fastapi import FastAPI, Request
from app.api import api_router
from app.core.database import engine, Base
from app.core.exceptions import NotFoundError, InvalidInputError
from app.utils.jsend import JSendResponse
from fastapi.middleware.cors import CORSMiddleware
from app.core.config import APP_VERSION, APP_NAME

app = FastAPI(
    title=APP_NAME,
    description='Esto es un proyecto diseñado en FastAPI',
    version=APP_VERSION)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
def startup_event():
    Base.metadata.create_all(bind=engine)


@app.on_event("shutdown")
def shutdown_event():
    pass


@app.exception_handler(NotFoundError)
async def not_found_exception_handler(request: Request, exc: NotFoundError):
    jsend_model = JSendResponse(status="fail", message=str(exc), status_code=400)
    return jsend_model


@app.exception_handler(InvalidInputError)
async def invalid_input_exception_handler(request: Request, exc: InvalidInputError):
    jsend_model = JSendResponse(status="fail", message=str(exc), status_code=400)
    return jsend_model


app.include_router(api_router)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
