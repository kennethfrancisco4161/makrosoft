class NotFoundError(Exception):
    pass


class InvalidInputError(Exception):
    pass


class HTTPException(Exception):
    pass
