from fastapi import APIRouter

from app.supports.Router import support_workers
from app.workers.Router import route_workers

api_router = APIRouter()


api_router.include_router(route_workers, prefix="/workers", tags=["workers"])
api_router.include_router(support_workers, prefix="/supports", tags=["supports"])
