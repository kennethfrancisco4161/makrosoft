from app.core.exceptions import NotFoundError


class RepositoryGeneric:
    def __init__(self, model, db):
        self.model = model
        self.db = db

    def add(self, entity):
        self.db.add(entity)
        return entity

    def get_by_id(self, id, id_column='id', raise_exception=True):
        data = self.db.query(self.model).filter_by(**{id_column: id}).first()
        if data is None and raise_exception:
            raise NotFoundError(f"Not found {id} in colum id {id_column} in model {self.model}")
        return data

    def get_all(self):
        return self.db.query(self.model).all()

    def update(self, entity):
        # self.db.commit()
        return entity

    def get_all_by_id(self, id, id_column='id'):
        return self.db.query(self.model).filter_by(**{id_column: id}).all()

    def delete(self, id, id_column='id'):
        data = self.db.query(self.model).filter_by(**{id_column: id}).first()
        self.db.delete(data)
        return data
