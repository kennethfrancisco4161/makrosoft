from app.repositories.RepositoryGeneric import RepositoryGeneric


class WorkerRepository(RepositoryGeneric):
    def get_worker_by_complexity(self):
        return self.db.query(self.model).order_by(self.model.complexity).all()
