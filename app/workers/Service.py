from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

from app.core.exceptions import NotFoundError
from app.models.Models import WorkersEntity
from app.workers.Repository import WorkerRepository


class Service:
    def __init__(self, db: Session):
        self.form_name = "workers"
        self.db = db
        self.repo = WorkerRepository(WorkersEntity, db)

    def get_all(self):
        try:
            lista = self.repo.get_all()
            return lista
        except SQLAlchemyError as e:
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def get_by_id(self, id_worker):
        try:
            worker = self.repo.get_by_id(id_worker, 'id')
            return worker
        except SQLAlchemyError as e:
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def save(self, payload):
        session = self.db
        try:
            with session.begin():
                person_to_create = WorkersEntity(**payload.model_dump())
                person_created = self.repo.add(person_to_create)
                return person_created.name_worker
        except SQLAlchemyError as e:
            session.rollback()
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def update(self, id_worker, payload):
        session = self.db
        try:
            with session.begin():
                person_exist = self.repo.get_by_id(id_worker, 'id')
                if person_exist is None:
                    raise NotFoundError(f"{self.form_name} Error {id_worker} not found.")
                else:
                    person_exist.name_worker = payload.name_worker
                    person_exist.last_name_worker = payload.last_name_worker

                    self.repo.update(person_exist)
                return True
        except SQLAlchemyError as e:
            session.rollback()
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def delete(self, id_worker):
        session = self.db
        try:
            with session.begin():
                person_exist = self.repo.get_by_id(id_worker, 'id')
                if person_exist is None:
                    raise NotFoundError(f"{self.form_name} Error {id_worker} not found.")
                else:
                    deleted_worker = self.repo.delete(person_exist.id, 'id')
                    return deleted_worker.name_worker
        except SQLAlchemyError as e:
            session.rollback()
            raise NotFoundError(f"{self.form_name} Error {e} not found.")
