from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.core.database import get_db
from app.workers.Service import Service
from app.utils.Utilities import ResponseMessages
from app.utils.jsend import JSendResponse
from app.workers.WorkerDTO import CreateWorker

route_workers = APIRouter()


@route_workers.get("/")
def get(db: Session = Depends(get_db)):
    service = Service(db)
    workers = service.get_all()
    return JSendResponse(status="success", message=ResponseMessages.SUCCESS_QUERY, data=workers)


@route_workers.get("/{id_worker}")
def get_by_id(id_worker: int, db: Session = Depends(get_db)):
    service = Service(db)
    person = service.get_by_id(id_worker)
    return JSendResponse(status="success", message=ResponseMessages.SUCCESS_QUERY, data=person)


@route_workers.post("/create")
def create_worker(payload: CreateWorker, db: Session = Depends(get_db)):
    service = Service(db)
    name = service.save(payload)
    return JSendResponse(status="success", message=ResponseMessages.CREATED, data=name)


@route_workers.put("/update/{id_worker}")
def update_worker(id_worker: int, payload: CreateWorker, db: Session = Depends(get_db)):
    service = Service(db)
    service.update(id_worker, payload)
    return JSendResponse(status="success", message=ResponseMessages.UPDATED, data=payload)


@route_workers.delete("/delete/{id_worker}")
def delete_worker(id_worker: int, db: Session = Depends(get_db)):
    service = Service(db)
    person = service.delete(id_worker)
    return JSendResponse(status="success", message=ResponseMessages.DELETED, data=person)
