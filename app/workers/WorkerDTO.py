from pydantic import BaseModel


class CreateWorker(BaseModel):
    name_worker: str
    last_name_worker: str

    class Config:
        from_attributes = True
