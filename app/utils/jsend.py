from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from typing import Union, List, Any


class JSendResponse(JSONResponse):
    def __init__(self,
                 status: str,
                 data: Union[List[BaseModel], BaseModel, Any] = None,
                 message=None,
                 status_code: int = 200,
                 *args,
                 **kwargs):
        content = {
            "status": status,
            "message": message
        }

        if status_code != 204:
            encoded_data = jsonable_encoder(data)
            content["data"] = encoded_data

        super().__init__(content=content if status_code != 204 else None, status_code=status_code, *args, **kwargs)
