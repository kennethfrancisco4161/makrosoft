from datetime import date

from pydantic import BaseModel


class CreateSupport(BaseModel):
    description: str
    complexity: int
    assignment_date: date

    class Config:
        from_attributes = True
