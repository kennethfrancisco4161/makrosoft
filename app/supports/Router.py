from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.core.database import get_db
from app.supports.Service import Service
from app.supports.SupportDTO import CreateSupport
from app.utils.Utilities import ResponseMessages
from app.utils.jsend import JSendResponse

support_workers = APIRouter()


@support_workers.get("/")
def get(db: Session = Depends(get_db)):
    service = Service(db)
    supports = service.get_all()
    return JSendResponse(status="success", message=ResponseMessages.SUCCESS_QUERY, data=supports)


@support_workers.get("/{id_support}")
def get_by_id(id_support: int, db: Session = Depends(get_db)):
    service = Service(db)
    supports = service.get_by_id(id_support)
    return JSendResponse(status="success", message=ResponseMessages.SUCCESS_QUERY, data=supports)


@support_workers.get("/report/{assignment_date}")
def get_report(assignment_date: str, db: Session = Depends(get_db)):
    service = Service(db)
    supports = service.get_all_report(assignment_date)
    return JSendResponse(status="success", message=ResponseMessages.SUCCESS_QUERY, data=supports)


@support_workers.post("/create_support")
def create_support(payload: CreateSupport, db: Session = Depends(get_db)):
    service = Service(db)
    supports = service.create_support(payload)
    return JSendResponse(status="success", message=ResponseMessages.CREATED, data=supports)


@support_workers.patch("/change_day")
def change_day(db: Session = Depends(get_db)):
    service = Service(db)
    service.change_day()
    return JSendResponse(status="success", message=ResponseMessages.UPDATED, data=ResponseMessages.CHANGE)
