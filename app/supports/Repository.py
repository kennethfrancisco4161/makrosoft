from app.repositories.RepositoryGeneric import RepositoryGeneric


class SupportRepository(RepositoryGeneric):
    pass


class ViewReportRepository(RepositoryGeneric):
    pass
