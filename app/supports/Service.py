from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

from app.core.exceptions import NotFoundError
from app.models.Models import SupportEntity, WorkersEntity
from app.models.ViewModel import ViewReportEntity
from app.supports.Repository import SupportRepository, ViewReportRepository
from app.workers.Repository import WorkerRepository


class Service:
    def __init__(self, db: Session):
        self.form_name = "supports"
        self.db = db
        self.repo = SupportRepository(SupportEntity, db)
        self.repo_worker = WorkerRepository(WorkersEntity, db)
        self.repoReport = ViewReportRepository(ViewReportEntity, db)

    def get_all(self):
        try:
            lista = self.repo.get_all()
            return lista
        except SQLAlchemyError as e:
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def get_by_id(self, id_support):
        try:
            support = self.repo.get_by_id(id_support, 'id')
            return support
        except SQLAlchemyError as e:
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def get_all_report(self, assignment_date):
        try:
            lista = self.repoReport.get_all_by_id(assignment_date, 'assignment_date')
            return lista
        except SQLAlchemyError as e:
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def create_support(self, payload):
        session = self.db
        try:
            with session.begin():
                lista_workers = self.repo_worker.get_worker_by_complexity()

                worker_exist = self.repo_worker.get_by_id(lista_workers[0].id, 'id')
                worker_exist.complexity += payload.complexity
                self.repo_worker.update(worker_exist)

                support_to_created = SupportEntity(**payload.model_dump())
                support_to_created.worker_id = worker_exist.id

                support_created = self.repo.add(support_to_created)

                return support_created.description
        except SQLAlchemyError as e:
            session.rollback()
            raise NotFoundError(f"{self.form_name} Error {e} not found.")

    def change_day(self):
        session = self.db
        try:
            with session.begin():
                lista_workers = self.repo_worker.get_all()

                for worker in lista_workers:
                    worker.complexity = 0
                    self.repo_worker.update(worker)
                return True
        except SQLAlchemyError as e:
            session.rollback()
            raise NotFoundError(f"{self.form_name} Error {e} not found.")
