from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import Column, Integer, String, Numeric, Date

Base = declarative_base()


class ViewReportEntity(Base):
    __tablename__ = 'view_report_group'

    id = Column(Numeric)
    name_worker = Column(String, primary_key=True)
    complexity = Column(Integer, default=0)
    assignment_date = Column(Date)
