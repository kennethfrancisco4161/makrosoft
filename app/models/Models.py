from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import Column, Integer, String, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

Base = declarative_base()


class WorkersEntity(Base):
    __tablename__ = 'workers'

    id = Column(Numeric, primary_key=True)
    name_worker = Column(String)
    last_name_worker = Column(String)
    complexity = Column(Integer, default=0)

    supports = relationship("SupportEntity")


class SupportEntity(Base):
    __tablename__ = 'supports'

    id = Column(Numeric, primary_key=True)
    description = Column(String)
    complexity = Column(Integer)
    assignment_date = Column(Date)
    worker_id = Column(Numeric, ForeignKey('workers.id'), nullable=False)
